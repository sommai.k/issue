package com.wl.issue;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/company")
@CrossOrigin(origins = "*")
public class CompanyController {
	
	@Autowired
	DataSource datasource;
	
	@GetMapping(value="")
	public ArrayList<Map<String, String>> getData(){
		ArrayList<Map<String, String>> result = new ArrayList<>();
		try {
			Connection _con = this.datasource.getConnection();
			Statement _stmt = _con.createStatement();
			ResultSet _res = _stmt.executeQuery("select * from tb_company");
			while(_res.next()) {
				Map<String, String> data = new HashMap<>();
				data.put("code", _res.getString("comp_code"));
				data.put("name", _res.getString("comp_name"));
				result.add(data);
			}
			_res.close();
			_stmt.close();
			_con.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return result;		
	}
	
	@PostMapping(value="")
	public Map<String, Object> addData( @RequestBody Map<String, String> body ){
		Map<String, Object> result = new HashMap<>();
		boolean success = true;
		try {
			Connection _con = this.datasource.getConnection();
			PreparedStatement _ins = _con.prepareStatement(
					" insert into tb_company (comp_code, comp_name)"+
					" values(?, ?)"
			);
			_ins.setString(1, body.get("code") );
			_ins.setString(2, body.get("name") );
			_ins.executeUpdate();
			_ins.close();
			_con.close();
		}catch(SQLException e) {
			success = false;
			result.put("message", e.getMessage());
			e.printStackTrace();
		}
		result.put("success", success);
		
		return result;		
	}
	
	@DeleteMapping(value="/{code}")
	public Map<String, Object> deleteData(@PathVariable String code){
		Map<String, Object> result = new HashMap<>();
		boolean success = true;
		try {
			Connection _con = this.datasource.getConnection();
			PreparedStatement _del = _con.prepareStatement(
				"delete from tb_company where comp_code = ?"
			);
			_del.setString(1, code );
			_del.executeUpdate();
			_del.close();
			_con.close();
		}catch(SQLException e) {
			success = false;
			result.put("message", e.getMessage());
			e.printStackTrace();
		}
		result.put("success", success);
		
		return result;		
	}
	
	@PutMapping(value="/{pkcode}")
	public Map<String, Object> updateData(
			@PathVariable String pkcode, 
			@RequestBody Map<String, String> body){
		Map<String, Object> result = new HashMap<>();
		boolean success = true;
		try {
			Connection _con = this.datasource.getConnection();
			PreparedStatement _upd = _con.prepareStatement(
				"update tb_company set comp_code = ?, comp_name = ? where comp_code = ?"
			);
			_upd.setString(1, body.get("code") );
			_upd.setString(2, body.get("name") );
			_upd.setString(3,  pkcode);
			_upd.executeUpdate();
			_upd.close();
			_con.close();
		}catch(SQLException e) {
			success = false;
			result.put("message", e.getMessage());
			e.printStackTrace();
		}
		result.put("success", success);
		
		return result;		
	}
	
	@PostMapping(value="/pro-update")
	public Map<String, Object> proUpdate( @RequestBody Map<String, String> body){
		Map<String, Object> result = new HashMap<>();
		Boolean success = true;
		try {
			Connection _con = this.datasource.getConnection();
			CallableStatement _pro = _con.prepareCall("{call pro_update(?, ?)}");
			_pro.setString(1, body.get("code") );
			_pro.setString(2, body.get("name") );
			_pro.execute();
			_pro.close();
			_con.close();
		}catch(SQLException e) {
			e.printStackTrace();
			success = false;
			result.put("message", e.getMessage());
		}
		result.put("success", success);
		return result;		
	}
	
	@PostMapping(value="/pro-update-array")
	public Map<String, Object> proUpdate( @RequestBody ArrayList<Map<String, String>> body){
		Map<String, Object> result = new HashMap<>();
		Boolean success = true;
		try {
			Connection _con = this.datasource.getConnection();
			CallableStatement _pro = _con.prepareCall("{call pro_update(?, ?)}");
			for(int i=0; i < body.size(); i++) {
				Map<String, String> data = body.get(i);
				_pro.setString(1, data.get("code") );
				_pro.setString(2, data.get("name") );
				_pro.addBatch();
			}
			_pro.executeBatch();
			_pro.close();
			_con.close();
		}catch(SQLException e) {
			e.printStackTrace();
			success = false;
			result.put("message", e.getMessage());
		}
		result.put("success", success);
		return result;		
	}
	
	@GetMapping(value="/concat-string/{var1}/{var2}")
	public Map<String, String> concatString( 
				@PathVariable String var1, 
				@PathVariable String var2
			){
		Map<String, String> result = new HashMap<>();
		try {
			Connection _con = this.datasource.getConnection();
			CallableStatement _pro = _con.prepareCall("{? = call concat_string(?, ?)}");
			_pro.registerOutParameter(1, Types.VARCHAR);
			_pro.setString(2, var1 );
			_pro.setString(3, var2 );
			_pro.execute();
			result.put("concat_string", _pro.getString(1) );
			_pro.close();
			_con.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return result;		
	}
	
}
