package com.wl.issue;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@Autowired
	DataSource datasource;
	
	@Value("${spring.datasource.url}")
	String url;
						
	@GetMapping( value="/hello" )
	public ArrayList<Map<String, String>> index(){
		ArrayList<Map<String, String>> result = new ArrayList<>();
		try {
			System.out.println("Connecting to "+this.url);
			Connection _con = this.datasource.getConnection();
			Statement _stmt = _con.createStatement();
			ResultSet _res = _stmt.executeQuery("select * from tb_company");
			while(_res.next()) {
				Map<String, String> data = new HashMap<>();
				data.put("code", _res.getString("comp_code"));
				data.put("name", _res.getString("comp_name"));
				result.add(data);
			}
			_res.close();
			_stmt.close();
			_con.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return result;		
	}
	
}
