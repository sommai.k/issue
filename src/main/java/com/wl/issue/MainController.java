package com.wl.issue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/api/v1/main")
public class MainController {
	
//	@RequestMapping(method = RequestMethod.GET, value="/index")
	@GetMapping("/index")
	public String index(Model model) {
		return "Hello World XXXX";
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/getName/{id}")
	public ArrayList<Map <String, Object>> getName(
			@PathVariable String id
		){
		System.out.println("id = "+id);
		ArrayList<Map <String, Object>> ret = new ArrayList<Map <String, Object>>();
		HashMap<String, Object> obj = new HashMap<String, Object>();
		obj.put("success", "true");
		obj.put("id", id);
		obj.put("name", "XXXXXX");
		ret.add(obj);
		
		return ret;
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/save")
	public Map <String, Object> save(
//		@RequestBody Map<String, Object> body,
		@RequestParam (value="name", required=false, defaultValue="World")   String name
//		@ModelAttribute	Map<String, String> frm
	){
		HashMap<String, Object> obj = new HashMap<String, Object>();
		obj.put("success", Boolean.TRUE);
		obj.put("name", name);
		
//		System.out.println(" Body code = "+ body.get("code"));
		return obj;
	}
}
