package com.wl.issue;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/login")
@CrossOrigin(origins = "*")
public class LoginController {

	@PostMapping("/doLogin")
	public Map<String, Object> doLogin(@RequestBody Map<String, Object> body) {
		HashMap<String, Object> obj = new HashMap<String, Object>();
		obj.put("success", Boolean.TRUE);
		obj.put("auth_token", "XXX");
		obj.put("userName", "Jame Bond");
		return obj;
	}
}
