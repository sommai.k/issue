FROM fabric8/maven-builder as builder
COPY . /app-src/
WORKDIR /app-src/

RUN mvn package

FROM tomcat:8.0-jre8-alpine
RUN cp /app-src/target/issue-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/issue.war
WORKDIR /usr/local/tomcat

EXPOSE 8080